#!/bin/sh

set -e

BASEDIR="$( cd "$(dirname "$0")" ; pwd -P )"

${BASEDIR}/../vendor/bin/drush --root=${BASEDIR}/../web updb -y
