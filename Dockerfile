#
# Stage 1 - the composer build process
#
FROM composer:1.8 as build-php-deps
WORKDIR /usr/src/app
COPY . ./
RUN composer global require hirak/prestissimo --no-plugins --no-scripts
# Running composer twice as post-install-cmd is buggy for now.
RUN composer install --ignore-platform-reqs --no-dev --no-interaction --no-progress || \
    composer install --ignore-platform-reqs --no-dev --no-interaction --no-progress

#
# Stage 2 - the production environment
#
FROM php:7.2-apache-stretch

# fix postgre install, see https://github.com/debuerreotype/debuerreotype/issues/10#issuecomment-450480318
RUN seq 1 8 | xargs -I{} mkdir -p /usr/share/man/man{}
RUN apt-get update -y
RUN apt-get install --no-install-recommends -y \
    libpng-dev \
    libpq-dev \
    postgresql \
    postgresql-contrib
RUN docker-php-ext-install \
    gd \
    opcache \
    mysqli \
    pdo \
    pdo_pgsql
RUN docker-php-ext-enable opcache
RUN rm -rf /var/lib/apt/lists/*

COPY --from=build-php-deps /usr/src/app /var/www/html
COPY ./provisioning/php.ini "/tmp/php.ini"
RUN mv "/tmp/php.ini" "$PHP_INI_DIR/php.ini"
COPY ./provisioning/apache-vh.conf /etc/apache2/sites-enabled/000-default.conf
COPY ./provisioning/opcache.ini $PHP_INI_DIR/conf.d/
RUN chmod 555 /var/www/html/web/sites/default
RUN chmod 555 /var/www/html/web/sites/default/settings.php
RUN chmod a+x /var/www/html/scripts/*.sh

EXPOSE 5000
